# Ninja Domain Service

![image](https://user-images.githubusercontent.com/106417552/188287412-42534087-ca3c-4b7b-9fa2-48a800914621.png)
A domain service build on the polygon mumbai testnet that lets users mint their own domain names as NFTs.

## Getting Started

This is a [React.js](https://reactjs.org/) project. 

Clone this repo! From there go ahead and run:
```
npm install
```

And then:
```
npm start
```

Open http://localhost:3000 with your browser to see the result.

# Basic Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts.

Try running some of the following tasks:

```shell
npm i hardhat
npx hardhat accounts
npx hardhat compile

npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```


```shell
npx hardhat compile
npx hardhat help
npx hardhat test
REPORT_GAS=true npx hardhat test
npx hardhat node

npx hardhat run --network mumbai scripts/deploy.js
npx hardhat run scripts/run.js

0x5FbDB2315678afecb367f032d93F642f64180aa3

Minted domain camid.nid
Set record for camid.nid
Owner of domain camid: 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266
Contract balance: 0.1

#  polygon: {
#       url: "RPC_URL",
#       accounts: [
#           "PRIVATE_KEY",
#       ],
#     },
#     ropsten: {
#         url: "https://ropsten.infura.io/v3/",
#         accounts: [
#             "PRIVATE_KEY",
#         ],
#     },
#     rinkeby: {
#         url: "https://eth-rinkeby.nodereal.io/v1/ID",
#         accounts: [
#             "PRIVATE_KEY",
#         ],
#         chainId: 4,
#         from: "ACCOUNT",
#         gas: 300000000000,
#     },
#     localhost: {
#         url: "http://127.0.0.1:8545/",
#         accounts: [
#           PRIVATE_KEY
#         ],
#     },

# contract first 
https://wiki.polygon.technology/docs/tools/ethereum/hardhat/
Downloading compiler 0.8.9
 Compiled 20 Solidity files successfully (evm target: london).

Contract deployed to: 0x2379D5A6D212bE2fF32d087F0aFCfEd27C62d9F0
Minted domain camid.nid
Set record for camid.nid
Owner of domain camid: 0x3C1fA707b08662214d6582adCF4A65f7864A2508
Contract balance: 0.1


#second 
Contract deployed to: 0x29FC8c3670D295d58F523038382f0a8F517F6572
Minted domain camid.nid
Set record for camid.nid
Owner of domain camid: 0x3C1fA707b08662214d6582adCF4A65f7864A2508
Contract balance: 0.1

#three 
inkhemarak@Ins-MacBook-Pro Ninja-Domain-Service % npx hardhat run --network polygon_mumbai scripts/deploy.js
Contract deployed to: 0xeC7818379F4141248EF27a230037255De868264c
Minted domain cambo.nid
Set record for cambo.nid
Owner of domain cambo: 0x3C1fA707b08662214d6582adCF4A65f7864A2508
Contract balance: 0.1
inkhemarak@Ins-MacBook-Pro Ninja-Domain-Service % 

Leng//
1
Owner of domain cambo: 0x5b893d140B642226b0BE8fd0c385cE0A8721fAa6
Contract deployed to: 0xe7e93DD1D022ca0C8172352bF3880C5898545D0F

2
Contract deployed to: 0x2c69aD47354d195Bb04E8233c8639235B833Dc2d
Owner of domain cambo: 0xE42fC4e7A1b4F78C12e25f3197e8c8bC1792CaBb



# new with logo
contract address: 0x3baDcF63734f1b9E47352A92C998fB1e00a52Be0
owner: 0x7554312e565290e6d335d1C3C53F8bf4CC925a9C

https://github.com/samailamalima/Domain_Serviceon_Polygon


0xDabbE474AAF9f7f799bA929f240697636DFC6c64